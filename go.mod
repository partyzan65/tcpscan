module gitlab.com/partyzan65/tcpscan

go 1.12

require (
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.3
)
