package main

import (
	"fmt"
	"log"
	"math"
	"net"
	"strings"
	"sync"

	"github.com/pkg/errors"
	"github.com/spf13/pflag"
)

var (
	ip    = pflag.String("ip", "127.0.0.1", "ip address")
	port  = pflag.Int("port", 0, "port number")
	start = pflag.Int("s", 1, "start port number")
	end   = pflag.Int("e", 65535, "end port number")
	t     = pflag.Int("t", 16, "parallel threads")
)

func main() {
	pflag.Parse()

	if *ip == "" {
		log.Fatal("ip address required")
	}

	if *port > 65535 {
		log.Fatalf("invalid port number %d", *port)
	}

	if *end > 65535 || *start > 65535 || *start > *end {
		log.Fatalf("invalid end or start port number: start %d, end %d", *start, *end)
	}

	if *port != 0 {
		res, err := connect(*ip, *port)
		if err != nil {
			log.Fatalf("connect failed: %s", err)
		}

		log.Printf("for ip %s port %d is opened=%v", *ip, *port, res)
		return
	}

	wg := &sync.WaitGroup{}
	wg.Add(*t)

	c := int(math.Ceil(float64(*end-*start) / float64(*t)))

	for j := 0; j < *t; j++ {
		s, e := j*c+1, (j+1)*c
		if e > *end {
			e = *end
		}

		go check(wg, s, e)
	}

	wg.Wait()

}

func check(wg *sync.WaitGroup, start, end int) {
	defer wg.Done()

	for i := start; i <= end; i++ {
		res, err := connect(*ip, i)
		if err != nil {
			log.Printf("%s:%d connect failed: %s", *ip, i, err)
		}

		if res {
			log.Printf("for ip %s port %d is opened", *ip, i)
		}
	}
}

func connect(ip string, port int) (bool, error) {
	addr := fmt.Sprintf("%s:%d", ip, port)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", addr)
	if err != nil {
		return false, errors.Wrap(err, "resolving ip tcp address failed")
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		if strings.Contains(err.Error(), "connection refused") {
			return false, nil
		}

		return false, errors.Wrap(err, "connection failed")
	}
	defer conn.Close()

	return conn != nil, nil
}
